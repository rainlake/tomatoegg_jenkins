#/bin/sh
make -C release/src-rt-6.x bin V1=$BUILD_NUMBER V2=-TomatoEgg OPENVPN=y NTFS=y BBEXTRAS=y USBEXTRAS=y EBTABLES=y MEDIASRV=y IPV6SUPP=y B=E BUILD_DESC="AIO" USB="USB" NOCAT=y NFS=y SNMP=y HFS=y DNSCRYPT=y UPS=y PPTPD=y TOR=y IPSEC=y RAID=y MIPS32=r2 NVRAM_64K=y NAND=y NO_JFFS=y AC66U=1 BTCLIENT=y ARIA2=y CTF=y PPPOE_RELAY=y PPPOE_SERVER=y AIRPLAY=y UFSD=y
make -C release/src-rt-6.x bin V1=$BUILD_NUMBER V2=-TomatoEgg OPENVPN=y NTFS=y BBEXTRAS=y USBEXTRAS=y EBTABLES=y MEDIASRV=y IPV6SUPP=y B=E BUILD_DESC="AIO" USB="USB" NOCAT=y NFS=y SNMP=y HFS=y DNSCRYPT=y UPS=y PPTPD=y TOR=y IPSEC=y RAID=y MIPS32=r2 NVRAM_64K=y W1800R=1  BTCLIENT=y ARIA2=y CTF=y PPPOE_RELAY=y PPPOE_SERVER=y AIRPLAY=y UFSD=y
#make linksys E series with 64k nvram
make -C release/src-rt bin V1=$BUILD_NUMBER V2=-TomatoEgg NTFS=y BBEXTRAS=y USBEXTRAS=y EBTABLES=y IPV6SUPP=y B=M BUILD_DESC="IPv6-Transmission-VPN-Airplay-8M" USB="USB" BTGUI=y NFS=y NOCAT=y OPENVPN=y DNSCRYPT=y PPTPD=y NO_JFFS=y NO_CIFS=y MIPS32=r2 NVRAM_SIZE=64 LINKSYS_E_64k=y PPPOE_RELAY=y BTCLIENT=y AIRPLAY=y

make -C release/src-rt bin V1=$BUILD_NUMBER V2=-TomatoEgg BBEXTRAS=y USBEXTRAS=y EBTABLES=y B=M BUILD_DESC="ARIA-8M" USB="USB" NOCAT=y DNSCRYPT=y PPTPD=y NO_JFFS=y NO_CIFS=y MIPS32=r2 NVRAM_SIZE=64 LINKSYS_E_64k=y PPPOE_RELAY=y ARIA2=y NO_FTP=y

make -C release/src-rt bin V1=$BUILD_NUMBER V2=-TomatoEgg NTFS=y MEDIASRV=y BBEXTRAS=y USBEXTRAS=y EBTABLES=y IPV6SUPP=y B=M BUILD_DESC="IPv6-MEDIASRV-VPN" USB="USB" BTGUI=y NFS=y NOCAT=y OPENVPN=y DNSCRYPT=y PPTPD=y NO_JFFS=y NO_CIFS=y MIPS32=r2 NVRAM_SIZE=64 LINKSYS_E_64k=y PPPOE_RELAY=y

#make linksys E series with 60k nvram
make -C release/src-rt bin V1=$BUILD_NUMBER V2=-TomatoEgg NTFS=y BBEXTRAS=y USBEXTRAS=y EBTABLES=y IPV6SUPP=y B=M BUILD_DESC="IPv6-Transmission-VPN-Airplay-8M" USB="USB" BTGUI=y NFS=y NOCAT=y OPENVPN=y DNSCRYPT=y PPTPD=y NO_JFFS=y NO_CIFS=y MIPS32=r2 NVRAM_SIZE=60 LINKSYS_E=y PPPOE_RELAY=y BTCLIENT=y AIRPLAY=y
make -C release/src-rt bin V1=$BUILD_NUMBER V2=-TomatoEgg BBEXTRAS=y USBEXTRAS=y EBTABLES=y B=M BUILD_DESC="ARIA-8M" USB="USB" NOCAT=y DNSCRYPT=y PPTPD=y NO_JFFS=y NO_CIFS=y MIPS32=r2 NVRAM_SIZE=60 LINKSYS_E=y PPPOE_RELAY=y ARIA2=y NO_FTP=yi
make -C release/src-rt bin V1=$BUILD_NUMBER V2=-TomatoEgg NTFS=y MEDIASRV=y BBEXTRAS=y USBEXTRAS=y EBTABLES=y IPV6SUPP=y B=M BUILD_DESC="IPv6-MEDIASRV-VPN" USB="USB" BTGUI=y NFS=y NOCAT=y OPENVPN=y DNSCRYPT=y PPTPD=y NO_JFFS=y NO_CIFS=y MIPS32=r2 NVRAM_SIZE=60 LINKSYS_E_64k=y PPPOE_RELAY=y
